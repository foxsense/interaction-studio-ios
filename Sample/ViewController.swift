//
//  ViewController.swift
//  Sample
//
//  Created by Gowrishankar Jayagopal on 07/07/21.
//

import UIKit
import Evergage

class ViewController: UIViewController {
    var evergage: Evergage = Evergage.sharedInstance()
    var label: UILabel = UILabel(frame: CGRect(x: 10, y: 10, width: 200, height: 100))
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        view.backgroundColor = .red;
        
        self.evergage.logLevel = EVGLogLevel.warn;
        self.evergage.userId = "gowrishankar@foxsense.io";
        self.evergage.setUserAttribute("IOS_DUMMY_1", forName: "lastAddedProduct")
        //self.evergage.userId = "ramb@accelerize360.com";
        //self.evergage.setUserAttribute("userName", forName: "GJayagopal");
        //self.evergage.setUserAttribute("phoneNumber", forName: "72000");
        
        self.evergage.start{(clientConfigurationBuilder) in
            clientConfigurationBuilder.account = "partneraccelerize360us";
            clientConfigurationBuilder.dataset = "ptiwari";
            clientConfigurationBuilder.usePushNotifications = true;
            clientConfigurationBuilder.useDesignMode = true;
        }
        
        print("Evergage configured");
        
        let evgItem = EVGProduct.init(id: "PRODUCT_ID_1");
        self.evergageScreen?.viewItem(evgItem);
        self.evergageScreen?.viewItemDetail(evgItem);
        print("Product Event Sent");
        self.evergageScreen?.trackAction("Hello World!!");
        self.evergageScreen?.add(toCart: EVGLineItem.init(productId: "PRODUCT_ID_1", productName: "Nike Shoes", price: 100, quantity: 10));
        /*self.evergageScreen?.purchase(EVGOrder.init(id: "IOS_1", lineItems: [EVGLineItem.init(item: evgItem, quantity: 1)], totalValue: 300));*/
        print("Event sent");
        
        let handler = {[weak self] (campaign: EVGCampaign) -> Void in
            self?.handleCampaign(campaign: campaign);
        }
        
        self.evergageScreen?.setCampaignHandler(handler, forTarget: "Event");
    }
    
    func handleCampaign(campaign: EVGCampaign) {
        print(campaign.data);
        
        guard let productName = campaign.data["name"] as? String
        else { return }
        if (productName.isEmpty) { return }
        
        self.evergageScreen?.trackImpression(campaign);
        label.text = productName;
        view.addSubview(label);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
